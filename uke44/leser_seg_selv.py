# Dette programmet leser seg selv fra fil og skriver
# ut noe til skjermen.
f = open('leser_seg_selv.py','r')

print(f.readline()) # Sjekk readline(), readlines()
# Se spesielt at med readline() strippes linjeskift bort.
f.close() # Vi må lukke den også. Den settes 