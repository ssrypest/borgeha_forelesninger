import random

from matplotlib import pyplot as plt

akse_x = [x for x in range(20)]
akse_y = [x//2 for x in akse_x]
akse_y2 = [(x//2)*1.3-1 for x in akse_x]

plt.plot(akse_x, akse_y,'b--')
plt.plot(akse_x, akse_y2)


# Forskjellig utseende på plotting
#plt.style.use('fivethirtyeight')
plt.style.use('ggplot')

# Du kan legge på navn på ting:
plt.xlabel('Alder')
plt.ylabel('Timelønn')
plt.title('Timelønn basert på alder...')


plt.show()








'''
# Vi må ha noe å skrive ut:
# Data: Én liste for x-aksen, to for y-aksen
akse_x = [x for x in range(0,20)] # x fra 0 til 19
akse_y = [x//2 for x in akse_x] # heltallsdivisjon

# Men denne er kanskje litt finere å se på:
#akse_y = [(x*30+random.randint(0,100)) for x in akse_x] # litt tilfeldig
# Den siste øker med andre ord gradvis, plutt noe tilfeldig.

# Enkel plotting av data: (plt.style.available)
plt.plot(akse_x,akse_y)
akse_y2 = [(x//2)*1.3-1 for x in akse_x]
plt.plot(akse_x,akse_y2, 'b')
# hva skjer hvis en bytter ut plot med scatter i forrige linje?
plt.show()

# Forskjellig utseende på plotting
#plt.style.use('fivethirtyeight')
#plt.style.use('ggplot')

# Du kan legge på navn på ting:
#plt.xlabel('Alder')
#plt.ylabel('Timelønn')
#plt.title('Timelønn basert på alder...')

# Enkel plotting av data:
#plt.plot(akse_x,akse_y) # Under legger jeg inn to, og manipulerer utseende.
#plt.show()
# Vis at en kan zoome og slikt i vinduet, og beklag fontstørrelsen

# Det kan gjøres mye mer avansert:
# https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_linestyle
'''
'''
akse_y2 = [(x*30+random.randint(200,300)) for x in akse_x] # En grafe til!
plt.plot(akse_x,akse_y, color='g', linestyle = '--', marker='.',
         linewidth = 3, label='Før klaging') # Legger inn de to aksene en skal plotte
plt.plot(akse_x,akse_y2, 'b', label="Etter klaging") # Samme x-akse, ulik y-verdi
plt.legend() # Vi vil vise hva grafene viser, med fargekoding
plt.show() # Viser begge grafene
'''