#import matplotlib
import matplotlib.pyplot as plt
import numpy as np

# Data for plotting
t = np.arange(0.0, 2.0, 0.01)
# t blir vektoren [0.00, 0.01, 0.02, 0.03,...1.99]

s = 1 + np.sin(2 * np.pi * t)
# her regnes funksjonen over for hver verdi av t og dette
# legges inn i vektoren s

fig, ax = plt.subplots()
# genererer grafen og returnerer en referanse til
# figuren (til fig) og en liste over aksene (ax)

ax.plot(t, s) # plotter selve grafen

# ax.set(xlabel='time (s)', ylabel='voltage (mV)',
# title='About as simple as it gets, folks')
ax.grid()
fig.savefig("test.png")
plt.show()